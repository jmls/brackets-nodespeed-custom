/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, window, brackets, $*/

define(function (require, exports, module) {
    'use strict';

    // Brckets modules
    var CommandManager = brackets.getModule('command/CommandManager'),
        Menus = brackets.getModule('command/Menus'),
        ProjectManager = brackets.getModule('project/ProjectManager'),
        AppInit = brackets.getModule('utils/AppInit'),
        Dialogs = brackets.getModule('widgets/Dialogs'),
        DefaultDialogs = brackets.getModule('widgets/DefaultDialogs'),
        ExtensionUtils = brackets.getModule('utils/ExtensionUtils'),
        NodeDomain = brackets.getModule('utils/NodeDomain'),
        DocumentManager = brackets.getModule('document/DocumentManager'),
        Mustache = brackets.getModule("thirdparty/mustache/mustache"),
        MainViewManager = brackets.getModule('view/MainViewManager'),
        profileTemplate = require('text!templates/profileTemplate.html'),
        path = '',
        isDir = false,
        profile = {},
        downloadCommand;

    var clipboard = require('node_modules/clipboard-js/clipboard');

    var domain_path = ExtensionUtils.getModulePath(module, 'node/nodespeedCustomDomain'),
        domain = new NodeDomain('nodespeedCustomDomain', domain_path);

    ExtensionUtils.loadStyleSheet(module, 'styles/style.less');

    function openPage(prefix) {

        domain.exec('getEnvVar', 'VIRTUAL_HOST').done(function (data) {

            var baseUrl = '',
                url = '',
                vars = [];

            if (data) {
                vars = data.split(',');
            }

            if (vars.length > 0) {
                baseUrl = vars[0].substring(0, vars[0].indexOf(':'));
                url = prefix + baseUrl;
                window.open('https://' + url);
            } else {
                Dialogs.showModalDialog(DefaultDialogs.DIALOG_ID_INFO, 'VIRTUAL_HOST', 'Environment variable not set.');
            }
        });
    }

    function copyToClipboard() {
        clipboard.copy({
            'text/plain': path
        });
    }

    function showTTY() {
        openPage('tty-');
    }

    function showPreview() {
        openPage('preview-');
    }

    function checkUnsavedFiles() {
        var openFiles = MainViewManager.getAllOpenFiles();
        var i;

        for (i = 0; i < openFiles.length; i++) {
            var doc = DocumentManager.getOpenDocumentForPath(openFiles[i].fullPath);
            if (doc && doc.isDirty) {
                return true;
            }
        }
        return false;
    }

    function getMessage() {
        return checkUnsavedFiles() ? 'You have unsaved files!' : 'You are about to leave your nodeSpeed environment.';
    }

    function downloadFiles() {

        window.onbeforeunload = null;

        if (isDir) {
            if (path[path.length - 1] === '/') {
                path = path.substring(0, path.length - 1);
            }
            window.location.href = '/download-folder?path=' + path;


        } else {

            window.location.href = '/download?path=' + path;

            setTimeout(function () {
                window.onbeforeunload = function () {
                    return getMessage();
                };
            }, 100);
        }
    }

    function showProfile() {
        Dialogs.showModalDialog(DefaultDialogs.DIALOG_ID_INFO, 'Profile', Mustache.render(profileTemplate, {
            profile: profile._json
        }));
    }

    function logout() {
        window.location.href = '/logout';
    }

    function restartProcess() {
        Dialogs.showModalDialog(
            DefaultDialogs.DIALOG_ID_EXT_DELETED, 'Node process restart', ((checkUnsavedFiles()) ? 'You have unsaved files! Are you sure you want to restart nodeSpeed process?' : 'Are you sure you want to restart the nodeSpeed process?'), [{
                    className: Dialogs.DIALOG_BTN_CLASS_NORMAL,
                    id: 'no',
                    text: 'No'
                }, {
                    className: Dialogs.DIALOG_BTN_CLASS_PRIMARY,
                    id: 'yes',
                    text: 'Yes'
                }]
            )
            .done(function (id) {
                if (id === 'yes') {
                    Dialogs.showModalDialog(DefaultDialogs.DIALOG_ID_INFO, 'Restarting..', 'Process restarted, refresh your browser page to continue working.');
                    domain.exec('exitProcess');
                }
            });
    }

    function formatNickname(nickname) {
        var temp = nickname.toLowerCase();
        return temp.charAt(0).toUpperCase() + temp.slice(1);
    }

    AppInit.htmlReady(function () {

        var GET_TTY = 'custom.getTTY';
        var GET_PREVIEW = 'custom.getPreview';
        var DOWNLOAD_FILES = 'custom.downloadFiles';
        var CLIPBOARD = 'custom.clipboard';
        var EXIT_PROCESS = 'custom.exitProcess';
        var LOGOUT = 'custom.logout';
        var GET_PROFILE = 'custom.getProfile';

        downloadCommand = CommandManager.register('Download..', DOWNLOAD_FILES, downloadFiles);
        downloadCommand.setEnabled(false);

        CommandManager.register('Terminal', GET_TTY, showTTY);
        CommandManager.register('Preview', GET_PREVIEW, showPreview);
        CommandManager.register('Copy path to clipboard..', CLIPBOARD, copyToClipboard);
        CommandManager.register('Restart nodeSpeed..', EXIT_PROCESS, restartProcess);

        var menu = Menus.getMenu(Menus.AppMenuBar.NAVIGATE_MENU);
        menu.addMenuDivider();
        menu.addMenuItem(GET_TTY);
        menu.addMenuItem(GET_PREVIEW);

        var debug_menu = Menus.getMenu('debug-menu');

        debug_menu.addMenuDivider();
        debug_menu.addMenuItem(EXIT_PROCESS);

        var context_menus = Menus.getContextMenu(Menus.ContextMenuIds.PROJECT_MENU);

        context_menus.addMenuDivider();
        context_menus.addMenuItem(CLIPBOARD);
        context_menus.addMenuItem(DOWNLOAD_FILES);

        $.ajax({
            type: 'GET',
            url: '/profile',
            dataType: 'json',
            success: function (data) {

                profile = data;

                var userMenu = Menus.addMenu(formatNickname(data.nickname), 'custom.user', Menus.AFTER, Menus.AppMenuBar.HELP_MENU);

                CommandManager.register('Logout', LOGOUT, logout);
                CommandManager.register('Profile', GET_PROFILE, showProfile);

                userMenu.addMenuItem(GET_PROFILE);
                userMenu.addMenuItem(LOGOUT);
            }
        });

        context_menus.on('beforeContextMenuOpen', function () {
            path = isDir = '';
            var selectedEntry = ProjectManager.getSelectedItem();

            if (selectedEntry) {
                path = selectedEntry._path;
                isDir = (selectedEntry._isDirectory) ? true : false;
                downloadCommand.setEnabled(true);
            } else {
                downloadCommand.setEnabled(false);
            }
        });

        window.onbeforeunload = function () {
            return getMessage();
        };
    });
});
