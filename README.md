# Brackets nodeSpeed custom #

Brackets extension to add custom features to nodeSpeed Development (Brackets in a Docker container and browser).

### Installation ###

This extension has to be installed manually into the users extension directory in a nodeSpeed Development instance:

```
cd /projects/.brackets-server/extensions/user
git clone git@github.com:whogloo/brackets-nodespeed-custom.git
cd brackets-nodespeed-custom
npm install
```

Once the extension has been installed, refresh the nodeSpeed Development instance with CTRL-F5. The nodeSpeed Development IDE is then extended with some custom features.

### Current custom features ###

* Added menu options to access instance settings to provide menu options for accessing a TTY Terminal session in a separate browser tab, and also to access an instance Preview in a separate browser tab.
* Added confirmation dialog when nodeSpeed Development is closed to prevent accidental closure of nodeSpeed Development.
* Context menu option on the treeview to copy folder/file path to clipboard.
