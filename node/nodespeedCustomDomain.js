/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50, white: true */
/*global require, exports*/

(function () {

    'use strict';

    var _domainManager;

    // Node Modules
    var fs = require('fs');

    /**
     * @private
     * Handler function for checking if file exists on disk.
     * @return {string} result.
     */
    function getEnvVar(name) {
        try {
            return process.env[name];
        } catch (e) {
            return e.message;
        }
    }

    /**
     * @private
     * Handler function to restart pnode process.
     */
    function exitProcess() {
        process.exit();
    }

    // Init
    function init(domainManager) {

        _domainManager = domainManager;

        if (!domainManager.hasDomain('nodespeedCustomDomain')) {

            domainManager.registerDomain('nodespeedCustomDomain', {
                major: 0,
                minor: 1
            });
        }

        _domainManager.registerCommand('nodespeedCustomDomain', 'getEnvVar', getEnvVar, false, 'Gets environment variable from server', [{
            name: 'name',
            type: 'string',
            description: 'Env variable name'
        }], [{
            name: 'value', // return value
            type: 'string',
            description: 'Env variable value'
        }]);

        _domainManager.registerCommand('nodespeedCustomDomain', 'exitProcess', exitProcess, false, 'Restarts node process', [], []);
    }

    exports.init = init;
}());
